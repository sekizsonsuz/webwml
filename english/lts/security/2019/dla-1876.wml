<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>In GOsa², an LDAP web-frontend written in PHP, a vulnerability was found
that could theoretically have lead to unauthorized access to the LDAP
database managed with FusionDirectory. LDAP queries' result status
("Success") checks had not been strict enough. The resulting output
containing the word <q>Success</q> anywhere in the returned data during login
connection attempts would have returned <q>LDAP success</q> to FusionDirectory
and possibly grant unwanted access.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.7.4+reloaded2-1+deb8u4.</p>

<p>We recommend that you upgrade your gosa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1876.data"
# $Id: $
