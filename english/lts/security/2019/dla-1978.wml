<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that python-ecdsa, a cryptographic signature library
for Python, did not correctly verify DER encoded signatures. Malformed
signatures could lead to unexpected exceptions and in some cases did
not raise any exception.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.11-1+deb8u1.</p>

<p>We recommend that you upgrade your python-ecdsa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1978.data"
# $Id: $
