<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability has been discovered in exiv2 (<a href="https://security-tracker.debian.org/tracker/CVE-2018-16336">CVE-2018-16336</a>), a C++
library and a command line utility to manage image metadata, resulting
in remote denial of service (heap-based buffer over-read/overflow) via
a crafted image file.</p>

<p>Additionally, this update includes a minor change to the patch for the
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10958">CVE-2018-10958</a>/<a href="https://security-tracker.debian.org/tracker/CVE-2018-10999">CVE-2018-10999</a> vulnerability first addressed in DLA
1402-1.  The initial patch was overly restrictive and has been adjusted
to remove the excessive restriction.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.24-4.1+deb8u2.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1551.data"
# $Id: $
