#use wml::debian::template title="Η Γωνιά των Προγραμματιστ(ρι)ών" 
#BARETITLE="true"
#use wml::debian::translation-check translation="2402c992a4bfcd9cacd2a5f22a50a77c20d716de" maintainer="galaxico"

<p>Οι πληροφορίες στη σελίδα αυτή, αν και δημόσιες, έχουν κυρίως ενδιααφέρον 
για Προγραμματιστές/προγραμματίστριες.</p>

<ul class="toc">
<li><a href="#basic">Βασικά</a></li>
<li><a href="#packaging">Πακετάρισμα</a></li>
<li><a href="#workinprogress">Έργα σε εξέλιξη</a></li>
<li><a href="#projects">Έργα (Project)</a></li>
<li><a href="#miscellaneous">Διάφορα</a></li>
</ul>

<div id="main">
  <div class="cardleft" id="basic">
  <h2>Βασικά</h2>
      <div>
      <dl>
        <dt><a href="$(HOME)/intro/organization">Η Οργάνωση του Debian</a></dt>

        <dd>
        Το Debian έχει πολλά σημεία πρόσβασης και εμπλέκει πολύ κόσμο. Η σελίδα 
αυτή εξηγεί με ποια άτομα πρέπει να επικοινωνήσετε για συγκεκριμένες πτυχές 
του Debian και σας λέει ποια ενδέχεται να απαντήσουν.
        </dd>

        <dt>Τα άτομα</dt>
        <dd>
        Το Debian παράγεται με συλλογική συνεργασία από άτομα που απλώνονται σ' 
ολόκληρο τον κόσμο. Στη <em>δουλειά σχετικά με το πακετάρισμα</em> συνεισφέρουν 
τόσο οι <a
        href="https://wiki.debian.org/DebianDeveloper">Προγραμματιστ(ρι)ες
        (DD)</a> (που είναι πλήρη μέλη του Σχεδίου Debian) και από <a
        href="https://wiki.debian.org/DebianMaintainer">Συντηρητές/τριες 
του Debian (DM)</a>. Εδώ μπορείτε να βρείτε και τη <a
        href="https://nm.debian.org/public/people/dd_all">λίστα των 
Προγραμματιστών/τριών του Debian</a> και τη <a
        href="https://nm.debian.org/public/people/dm_all">λίστα των 
Συντηρητών/τριών του Debian
        </a>, μαζί με τα πακέτα που συντηρούν.

        <p>
        Μπορείτε επίσης να δείτε τον <a 
href="developers.loc">παγκόσμιο χάρτη των προγραμματιστών/στριών του Debian
        </a> καθώς και <a href="https://gallery.debconf.org/">συλλογές 
φωτογραφιών</a> από διάφορες εκδηλώσεις σχετικές με το Debian.
        </p>
        </dd>

        <dt><a href="join/">Μπείτε στο Debian</a></dt>

        <dd>
        Το Σχέδιο Debian συγκροτείται από εθελοντές και γενικά ψάχνουμε για 
        καινούριους/ες προγραμματιστές/προγραμματίστριες που να έχουν μια 
        κάποια τεχνική γνώση, ενδιαφέρον για το ελεύθερο λογισμικό και λίγο 
        ελεύθερο χρόνο. Μπορείτε επίσης να βοηθήσετε το Debian, δείτε απλά τον 
σύνδεσμο στη σελίδα παραπάνω.
        </dd>

        <dt><a href="https://db.debian.org/">Βάση Δεδομένων των 
προγραμματίστ(ρι)ών </a></dt>
        <dd>
        Αυτή η βάση δεδομένων περιέχει κάποια βασικά δεδομένα προσβάσιμα σε 
όλους, και πιο προσωπικά δεδομένα που είναι διαθέσιμα μόνο στους άλλους/άλλες 
προγραμματιστές/προγραμματίστριες για να τα δουν. Χρησιμοποιήστε την 
        <a href="https://db.debian.org/">SSL έκδοση</a> για πρόσβαση, αν θέλετε 
να μπείτε στη βάση.

        <p>Χρησιμοποιώντας τη βάση, μπορείτε να δείτε τη λίστα των 
        <a href="https://db.debian.org/machines.cgi">μηχανημάτων του 
Σχεδίου</a>,
        <a href="extract_key">να πάρετε το GPG κλειδί 
οποιουδήποτε/οποιασδήποτε προγραμματιστή/προγραμματίστριας</a>,
        <a href="https://db.debian.org/password.html">να αλλάξετε τον 
κωδικό πρόσβασής σας</a>
        ή να <a href="https://db.debian.org/forward.html">μάθετε πώς να 
ρυθμίσετε την προώθηση αλληλογραφίας</a> για τον λογαριασμό σας στο Debian.</p>

        <p>Αν πρόκειται να χρησιμοποιήσετε ένα από τα μηχανήματα του Debian 
βεβαιωθείτε ότι έχετε διαβάσει τις <a href="dmup">Πολιτικές χρήσης των 
μηχανημάτων του Debian</a>.</p>
        </dd>

        <dt><a href="constitution">Το Καταστατικό</a></dt>
        <dd>
        Το πλέον σημαντικό κείμενο για τον οργανισμό του Debian, περιγράφει την 
οργανωτική δομή για την τυπική διαδικασία λήψης αποφάσεων στο Σχέδιο.
        </dd>

        <dt><a href="$(HOME)/vote/">Πληροφορίες για τις Ψηφοφορίες</a></dt>
        <dd>
        Οτιδήποτε θα θέλατε να ξέρετε για το πώς εκλέγουμε τον/την Επικεφαλής 
μας, επιλέγουμε τα λογότυπά μας και, γενικά, για το πώς ψηφίζουμε.
        </dd>
     </dl>

# this stuff is really not devel-only
     <dl>
        <dt><a href="$(HOME)/releases/">Εκδόσεις</a></dt>

        <dd>
        Αυτή είναι μια λίστα με τις παλιές και τρέχουσες εκδόσεις, για μερικές 
από τις οποίες υπάρχουν λεπτομερείς πληροφορίες σε ξεχωριστές σελίδες.

        <p>Μπορείτε επίσης να πάτε απευθείας στις ιστοσελίδες της τρέχουσας 
        <a href="$(HOME)/releases/stable/">σταθερής έκδοσης</a> και της
        <a href="$(HOME)/releases/testing/">δοκιμαστικής διανομής</a>.</p>
        </dd>

        <dt><a href="$(HOME)/ports/">Διαφορετικές αρχιτεκτονικές</a></dt>

        <dd>
        Το Debian τρέχει σε πολλά είδη υπολογιστών (οι συμβατοί 
με Intel ήταν απλά το <em>πρώτο</em> είδος), και οι συντηρητές των διαφόρων 
        &lsquo;υλοποιήσεων&rsquo; έχουν κάποιες χρήσιμες ιστοσελίδες. Ρίξτε μια 
ματιά, ίσως θελήσετε να αποκτήσετε ένα άλλο μηχάνημα με περίεργο όνομα για τον 
εαυτό σας.
	</dd>
      </dl>
      </div>

  </div>

  <div class="cardright" id="packaging">
     <h2>Πακετάρισμα</h2>
     <div>

      <dl>
        <dt><a href="$(DOC)/debian-policy/">Εγχειρίδιο πολιτικής 
του Debian</a></dt>
        <dd>
        Αυτό το εγχειρίδιο περιγράφει την απαιτήσεις πολιτικής για τη 
διανομή του Debian. Αυτό περιλαμβάνει την δομή και τα περιεχόμενα της 
αρχειοθήκης του Debian, αρκετά ζητήματα σχεδιασμού του λειτουργικού 
συστήματος καθώς και τεχνικές απαιτήσεις που το κάθε πακέτο θα πρέπει να 
ικανοποιεί για να συμπεριληφθεί στη διανομή.

        <p>Με λίγα λόγια, <strong>πρέπει</strong> να το διαβάσετε.</p>
        </dd>
      </dl>

      <p>Υπάρχουν αρκετά κείμενα σχετικά με την Πολιτική που ίσως θα σας 
ενδιέφεραν, όπως τα::</p>
      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />Το FHS είναι μια λίστα καταλόγων (ή αρχείων) όπου πρέπει να 
τοποθετηθούν διάφορα πράγματα, και η Πολιτική έκδοση 3.x απαιτεί τη συμβατότητα 
με αυτό.</li>
        <li>Λίστα <a 
href="$(DOC)/packaging-manuals/build-essential">build-essential πακέτων</a>
        <br />Τα πακέτα στη σουίτα build-essential είναι πακέτα που θα πρέπει 
να διαθέτετε πριν προσπαθήσετε να φτιάξετε οποιοδήποτε πακέτο ή σύνολο πακέτων 
που δεν χρειάζεται να το προσθέσετε στη γραμμή             
<code>Build-Depends</code> του πακέτου σας.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">Μενού συστήματος</a>
        <br />Προγράμματα που έχουν μια διεπαφή στην οποία δεν χρειάζεται να περαστούν οποιαδήποτε ειδικά ορίσματα γραμμής εντολών ώστε να λειτουργούν κανονικά, θα πρέπει να έχουν καταχωρημένη μια είσοδο στο μενού.
            Ελέγξτε επίσης και την <a href="$(DOC)/packaging-manuals/menu.html/">τεκμηρίωση του μενού του συστήματος</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Πολιτική του Emacs</a>
        <br />Τα πακέτα που σχετίζονται με τον Emacs υποτίθεται ότι θα πρέπει να συμμορφώνονται με το κείμενο της δικής τους υπο-πολιτικής.</li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Πολιτική Java</a>
        <br />Το προτεινόμενο ισοδύμανο του παραπάνω, για τα σχετιζόμενα με την Java πακέτα.</li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Πολιτική Perl</a>
        <br />A sub-policy that covers everything regarding Perl packaging.</li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Πολιτική Python</a>
        <br />A proposed sub-policy that covers everything regarding Python packaging.</li>
#	<li><a href="https://pkg-mono.alioth.debian.org/cli-policy/">Πολιτική του Debian για την CLI</a>
#	<br />Basic policies regarding packaging Mono, other CLRs and
#        CLI based applications and libraries</li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Προδιαγραφές του Debconf</a>
        <br />Οι προδιαγραφές για το υποσύστημα διαχείρισης ρυθμίσεων
            "debconf".</li>
#        <li><a href="https://dict-common.alioth.debian.org/">Spelling dictionaries and tools policy</a>
#        <br />Sub-policy for <kbd>ispell</kbd> / <kbd>myspell</kbd> dictionaries and word lists.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft/html/">Webapps Policy Manual</a> (draft)
#	<br />Sub-policy for web-based applications.</li>
#        <li><a href="https://webapps-common.alioth.debian.org/draft-php/html/">PHP Policy</a> (draft)
#	<br />Packaging standards of PHP.</li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Database Application Policy</a> (draft)
	<br />A set of guidelines and best practices for database application packages.</li>
	<li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk Policy</a> (draft)
	<br />Sub-policy that covers everything regarding Tcl/Tk packaging.</li>
	<li><a
	href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debian
	policy for Ada</a>
	<br />Sub-policy that covers everything regarding Ada packaging.</li>
      </ul>

      <p>Take a look at <a href="https://bugs.debian.org/debian-policy">
      proposed updates to Policy</a>, too.</p>

      <p>Note that the old Packaging Manual has mostly been integrated into
      the recent versions of the Policy Manual.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">
        Αναφορά των Προγραμματιστ(ρι)ών</a></dt>

        <dd>
        The purpose of this document is to provide an overview of the
        recommended procedures and the available resources for Debian
        developers. Another must-read.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Οδηγός Νέων 
Συντηρητ(ρι)ών</a></dt>

        <dd>
        This document describes building of a Debian package in common
        language, and is well covered with working examples. If you are a
        wannabe developer (packager), you will most definitely want to read
        this.
        </dd>
      </dl>
      </div>

  </div>

  <div class="card" id="workinprogress">
      <h2>Δουλειά&nbsp;σε&nbsp;εξέλιξη</h2>
      <div>

	<dl>
        <dt><a href="testing">Η δοκιμαστική διανομή</a></dt>
        <dd>
        The &lsquo;testing&rsquo; distribution is where you need to get your packages
        in order for them to be considered for releasing next time Debian
        makes a release.
        </dd>

        <dt><a 
href="https://bugs.debian.org/release-critical/">Σφάλματα κρίσιμα για 
την κυκλοφορία της έκδοσης</a></dt>

        <dd>
        This is a list of bugs which may cause a package to be removed
        from the "testing" distribution, or in some cases even cause a delay
        in releasing the distribution. Bug reports with a severity higher
        than or equal to &lsquo;serious&rsquo; qualify for the list -- be sure to fix
        any such bugs against your packages as soon as you can.
        </dd>

        <dt><a href="$(HOME)/Bugs/">Το Σύστημα Παρακολούθησης Σφαλμάτων</a></dt>
        <dd>
        The Debian Bug Tracking System (BTS) itself, for reporting,
        discussing, and fixing bugs. Reports of problems in almost any part
        of Debian are welcome here. The BTS is useful for both users and
        developers.
        </dd>

        <dt>Package overviews, from a developer's point of view</dt>
        <dd>
	The <a href="https://qa.debian.org/developer.php">package information</a>
        and <a href="https://tracker.debian.org/">package tracker</a> web pages
        provide collections of valuable information to maintainers.
        </dd>

        <dt><a
href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">Το 
Σύστημα Παρακολούθησης πακέτων του Debian</a></dt>
        <dd>
        For developers that wish to keep up-to-date with other packages, the
        package tracker allows them to subscribe (through email) to a
        service that will send them copies of BTS mails and notifications
        for uploads and installations concerning the packages subscribed to.
        </dd>

        <dt><a href="wnpp/">Πακέτα που χρειάζονται βοήθεια</a></dt>
        <dd>
        Work-Needing and Prospective Packages, WNPP for short, is a list
        of Debian packages in need of new maintainers, and also the packages
        that have yet to be included in Debian. Check it out if you want to
        create, adopt or orphan packages.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
            Σύστημα Εισερχομένων</a></dt>
        <dd>
        New packages are uploaded into the "Incoming" system on the internal
        archive servers. Accepted packages are almost immediately
        <a href="https://incoming.debian.org/">available via HTTP</a>,
        and propagated to <a href="$(HOME)/mirror/">mirrors</a> four times
        a day.
        <br />
        <strong>Note</strong>: Due to the nature of Incoming, we do
        not recommend mirroring it.
        </dd>

        <dt><a href="https://lintian.debian.org/">Αναφορές Lintian</a></dt>

        <dd>
        <a href="https://packages.debian.org/unstable/devel/lintian">
        Lintian</a> is a program that checks whether a package conforms
        to the Policy. You should use it before every upload;
        there are reports on the aforementioned page about every package in
        the distribution.
        </dd>

        <dt><a href="https://wiki.debian.org/HelpDebian">Βοηθήστε το 
Debian</a></dt>
        <dd>
	The Debian wiki gathers advice for developers and other contributors.
        </dd>

        <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">\
            Η πειραματική διανομή</a></dt>
        <dd>
        The <em>experimental</em> distribution is used as a temporary
        staging area for highly experimental software. Use the
        <a href="https://packages.debian.org/experimental/">packages from
        <em>experimental</em></a> only if you already know how to use
        <em>unstable</em>.
        </dd>
      </dl>
      </div>

  </div>
  <div class="card" id="projects">
     <h2>Έργα</h2>
     <div>

      <p>Debian is a large group, and as such, it consists of several
      internal groups and projects. Here are those that have web pages,
      sorted chronologically:</p>
      <ul>
          <li><a href="website/">Ιστοσελίδες του Debian</a></li>
          <li><a href="https://ftp-master.debian.org/">Αρχειοθήκη 
του Debian</a></li>
          <li><a href="$(DOC)/ddp">Το Σχέδιο Τεκμηρίωσης του 
Debian (DDP)</a></li>
          <li><a href="https://qa.debian.org/">Η ομάδα Διασφάλισης Ποιότητας</a>
              group</li>
          <li><a href="$(HOME)/CD/">Εικόνες CD του Debian</a></li>
          <li>The <a href="https://wiki.debian.org/Keysigning">key signing
              coordination page</a></li>
          <li><a href="https://wiki.debian.org/DebianIPv6">Σχεδιο IPv6 του 
Debian </a></li>
          <li><a href="buildd/">Auto-builder network</a> and their <a href="https://buildd.debian.org/">build logs</a></li>
          <li><a href="$(HOME)/international/l10n/ddtp">Debian Description Translation Project
              (DDTP)</a></li>
	  <li><a href="debian-installer/">Ο Εγκαταστάτης του Debian</a></li>
	  <li><a href="debian-live/">Debian Live</a></li>
	  <li><a href="$(HOME)/women/">Γυναίκες στο Debian</a></li>

	</ul>

	<p>Ένας αριθμός από αυτά τα έργα σκοπεύουν να δημιουργήσουν <a 
href="https://wiki.debian.org/DebianPureBlends">Καθαρά Μεγματα Debian
	</a> για μια συγκεκριμένη ομάδα χρηστών ενώ συνεχίζουν να δουλεύουν εξ 
ολοκλήρου μέσα σε ένα σύστημα Debian. Αυτά περιλαμβάνουν τα:</p>

	<ul>
	  <li><a href="debian-jr/">Σχέδιο Debian Jr</a></li>
          <li><a href="debian-med/">Σχέδιο Debian Med</a></li>
          <li><a href="https://wiki.debian.org/DebianEdu">Σχέδιο Debian 
Edu/Skolelinux</a></li>
	  <li><a href="debian-accessibility/">Σχέδιο Προσβασιμότητας του 
Debian</a></li>
	  <li><a href="https://wiki.debian.org/DebianGis">Σχέδιο Debian GIS</a></li>
	  <li><a href="https://wiki.debian.org/DebianScience">Σχέδιο Debian 
Science</a></li>
	  <li><a 
href="https://salsa.debian.org/debichem-team/team/debichem/">Σχέδιο 
DebiChem</a></li>
	</ul>
	</div>

  </div>

  <div class="card" id="miscellaneous">
      <h2>Διάφορα</h2>
      <div>

      <p>Assorted links:</p>
      <ul>
        <li><a 
href="https://debconf-video-team.pages.debian.net/videoplayer/">Βιντεοσκοπήσεις 
</a> των ομιλιών στα συνέδριά μας.</li>
        <li><a href="passwordlessssh">Ρυθμίστε το ssh ώστε να μην σας ζητά 
κωδικό πρόσβασης</a>.</li>
        <li> <a href="$(HOME)/MailingLists/HOWTO_start_list">Πώς να αιτηθείτε 
μια καινούρια λίστα αλληλογραφίας</a>.</li>
        <li> <a href="https://dsa.debian.org/iana/">Ιεραρχία OID του Debian
            </a>.</li>
        <li>Πληροφορίες για τους <a href="$(HOME)/mirror/">καθρεφτισμούς του 
Debian</a>.</li>
        <li> <a 
href="https://qa.debian.org/data/bts/graphs/all.png">Το γράφημα όλων των 
σφαλμάτων</a>.</li>
	<li><a href="https://ftp-master.debian.org/new.html">Καινούρια πακέτα σε 
αναμονή να συμπεριληφθούν στο Debian</a> (NEW queue).</li>
        <li><a 
href="https://packages.debian.org/unstable/main/newpkg">Καινούρια πακέτα 
Debian από τις τελευταίες 7 μέρες</a>.</li>
        <li><a href="https://ftp-master.debian.org/removals.txt">Πακέτα που 
αφαιρέθηκαν από το Debian</a>.</li>
        </ul>

      </div>

  </div>
</div>
