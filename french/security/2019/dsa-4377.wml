#use wml::debian::translation-check translation="5ab30aafbfd62a548582887f297aa136a1d43e7b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>L'équipe de sécurité d'ESnet a découvert une vulnérabilité dans rssh,
une invite de commande restreinte qui permet aux utilisateurs de ne
réaliser que des opérations scp, sftp, cvs, svnserve (Subversion), rdist
ou rsync. L'absence de validation dans la prise en charge de scp pourrait
avoir pour conséquence le contournement de cette restriction, permettant
l'exécution de commandes d'interpréteur arbitraires.</p>

<p>Veuillez noter qu'avec l'application de cette mise à jour, l'option
<q>-3</q> de scp ne peut plus être utilisée.</p>

<p>Pour la distribution stable (Stretch), ce problème a été corrigé dans la
version 2.3.4-5+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rssh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rssh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rssh">\
https://security-tracker.debian.org/tracker/rssh</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4377.data"
# $Id: $
