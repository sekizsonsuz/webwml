#use wml::debian::translation-check translation="0379f4af79a9d051cd0d13dbe731389aa48d6ce2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8615">CVE-2016-8615</a>

<p>Si l'état du cookie est écrit dans un fichier de conteneur à cookies qui
est relu plus tard et utilisé pour des requêtes postérieures, un serveur
HTTP malveillant peut injecter de nouveaux cookies pour des domaines
arbitraires dans ledit conteneur à cookies. Le problème se rapporte à la
fonction qui charge les cookies en mémoire, qui lit le fichier spécifié
dans un tampon à taille fixe ligne par ligne avec la fonction
<q>fgets()</q>. Si une invocation de fgets() ne peut pas lire la ligne
entière dans le tampon de destination parce qu'il est trop petit, il
tronque la sortie. De cette manière, un très long cookie (nom + valeur)
envoyé par un serveur malveillant serait stocké dans le fichier et ensuite
ce cookie pourrait être lu partiellement et contrefait correctement, il
pourrait être traité comme un cookie différent pour un autre serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8616">CVE-2016-8616</a>

<p>Lors de la réutilisation d'une connexion, curl effectuait des
comparaisons de noms d'utilisateur et de mots de passe avec les connexions
existantes sans tenir compte de la casse. Cela signifie que si une
connexion inutilisée avec des accréditations correctes existe pour un
protocole qui possède des accréditations au niveau de la connexion, un
attaquant peut provoquer la réutilisation de la connexion s'il connaît la
version insensible à la casse du bon mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8617">CVE-2016-8617</a>

<p>Dans la fonction d'encodage en base64 de libcurl, le tampon de sortie
est alloué comme suit sans vérification de la taille d'entrée :
malloc(taille d'entrée*4/3+4). Dans les systèmes avec des adresses 32 bits
dans l'espace utilisateur (par exemple x86, ARM, x32), la multiplication
dans l'expression cause un dépassement si la taille d'entrée était d'au
moins 1 Go de données. Si cela se produit, un tampon de sortie
sous-dimensionné sera alloué, mais le résultat complet sera écrit,
provoquant donc l'écrasement de la mémoire au-delà du tampon de sortie. Les
systèmes avec les versions 64 bits du type <q>size_t</q> ne sont pas
affectés par ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8618">CVE-2016-8618</a>

<p>La fonction de l'API appelée <q>curl_maprintf()</q> peut être contrainte
à une double libération de zone de mémoire à cause d'une multiplication de
<q>size_t</q> non sûre, dans les systèmes utilisant des variables
size_t de 32 bits. La fonction est aussi utilisée en interne dans de
nombreuses situations. Les systèmes avec les versions 64 bits du type
<q>size_t</q> ne sont pas affectés par ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8619">CVE-2016-8619</a>

<p>Dans l'implémentation de curl du mécanisme d'authentification Kerberos,
la fonction <q>read_data()</q> dans security.c est utilisée pour remplir
les structures krb5 nécessaires. Lors de la lecture d'un des champs de
longueur à partir d'une socket, il échoue à assurer que le paramètre de
longueur passé à realloc() n'est pas réglé à 0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8621">CVE-2016-8621</a>

<p>La fonction <q>curl_getdate</q> convertit une chaîne de date donnée en
un horodatage numérique et prend en charge une gamme de formats et de
possibilités divers pour exprimer une date et une heure. La fonction
d'analyse de date sous-jacente est aussi utilisée en interne par exemple lors de
l'analyse de cookies HTTP (reçues éventuellement de serveurs
distants) et il peut être utilisé lors de la réalisation de requêtes HTTP
conditionnelles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8622">CVE-2016-8622</a>

<p>La fonction de décodage d'URL <q>percent-encoding</q> dans libcurl est
appelée <q>curl_easy_unescape</q>. En interne, même si cette fonction
pouvait être faite pour allouer un tampon de destination avec une URL en
texte simple (<q>unescape</q> de plus de 2 Go, elle renverrait cette
nouvelle longueur dans une variable d'entier signée de 32 bits, donc la
longueur serait soit juste tronquée soit à la fois tronquée et devenue
négative. Cela pourrait mener libcurl à écrire en dehors de son tampon de
tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8623">CVE-2016-8623</a>

<p>libcurl permet de façon explicite à des utilisateurs de partager des
cookies entre plusieurs identifiants rapides qui sont employés
concurremment par différents fils. Quand les cookies à envoyer à un serveur
sont collectés, la fonction correspondante collecte tous les cookies à
envoyer et le verrou de cookie est libéré immédiatement après. Cette
fonction ne renvoie cependant qu'une liste avec les *références* aux
chaînes originales pour le nom, la valeur, etc. Donc, si un autre fil
prend rapidement le verrou et libère une des structures de cookie
originales avec ses chaînes, une utilisation de mémoire après libération
peut survenir et mener à la divulgation d'informations. Un autre fil peut
aussi remplacer les contenus des cookies à partir de réponses HTTP
distinctes ou d'appels d'API.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8624">CVE-2016-8624</a>

<p>curl n'analysait pas correctement le composant d'autorité de l'URL quand
la partie nom d'hôte se termine par un caractère <q>#</q>, et pourrait
plutôt être piégé dans une connexion à un hôte différent. Cela peut avoir
des implications de sécurité lorsqu'est utilisé, par exemple, un analyseur
d'URL qui suit le RFC pour vérifier les domaines autorisés avant d'utiliser
curl pour les demander.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 7.26.0-1+wheezy17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-711.data"
# $Id: $
