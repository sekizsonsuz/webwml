#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Z shell.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1071">CVE-2018-1071</a>

<p>Dépassement de pile dans la fonction exec.c:hashcmd(). Un attaquant local
pourrait exploiter cela pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1083">CVE-2018-1083</a>

<p>Dépassement de tampon dans la fonction autocomplete de l’interpréteur. Un
utilisateur local non privilégié peut créer un chemin de répertoire contrefait
pour l'occasion qui pourrait aboutir à une exécution de code dans le contexte
de l’utilisateur voulant autocompléter pour traverser le chemin sus-mentionné.
Si l’utilisateur à des privilèges, cela conduit à une élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.3.17-1+deb7u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets zsh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1335.data"
# $Id: $
