#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8365">CVE-2015-8365</a>

<p>La fonction smka_decode_frame dans libavcodec/smacker.c ne vérifiait pas que
la taille des données était cohérente avec le nombre de canaux. Cela permet à des
attaquants distants de provoquer un déni de service (accès tableau hors limites)
ou éventuellement d’avoir un impact non précisé à l’aide de données Smacker
contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7208">CVE-2017-7208</a>

<p>La fonction decode_residual dans libavcodec permet à des attaquants distants
de provoquer un déni de service (lecture excessive de tampon) ou d’obtenir des
informations sensibles de la mémoire du processus à l'aide d'un fichier vidéo h264
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7862">CVE-2017-7862</a>

<p>La fonction decode_frame dans libavcodec/pictordec.c est vulnérable à une
écriture hors limites provoquée par un dépassement de tampon basé sur le tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9992">CVE-2017-9992</a>

<p>La fonction decode_dds1 dans libavcodec/dfa.c permet à des attaquants
distants de provoquer un déni de service (dépassement de tampon basé sur le tas
et plantage d'application) ou éventuellement d’avoir un impact non précisé à
l'aide d'un fichier contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 6:0.8.21-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1142.data"
# $Id: $
