#use wml::debian::translation-check translation="8f53bcba8776bf82428e14a66b85e746f7f2e545" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découvertes dans libvpx, un codec vidéo VP8 et
VP9.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9232">CVE-2019-9232</a>

<p>Une lecture hors limites est possible à cause d’une vérification de limites
manquante. Cela pourrait conduire à une divulgation d'informations distantes sans
besoin de privilèges supplémentaires. Une interaction de l’utilisateur n’est pas
nécessaire pour son exploitation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9433">CVE-2019-9433</a>

<p>Une divulgation d'informations est possible à cause d’une validation
incorrecte d’entrées. Cela pourrait conduire à une divulgation d'informations
distantes sans besoin de privilèges supplémentaires. Une interaction de
l’utilisateur n’est pas nécessaire pour son exploitation.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.0-3+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvpx.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2012.data"
# $Id: $
