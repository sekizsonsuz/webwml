#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>D’autres vulnérabilités ont été trouvées par Nick Cleaton dans le code de
rssh qui pourraient conduire à l'exécution de code arbitraire dans certaines
circonstances.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3463">CVE-2019-3463</a>

<p>Rejet des options en ligne de commande de rsync --daemon et --config ;
exécution de commande arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3464">CVE-2019-3464</a>

<p>Empêchement pour popt de charger un fichier de configuration ~/.popt,
qui conduisait à l’exécution de commande arbitraire.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.3.4-4+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rssh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>


# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1660.data"
# $Id: $
