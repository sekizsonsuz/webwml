#use wml::debian::translation-check translation="8ce24a8757eea2129e97c895ce1eb6f1605eb1ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs chercheurs ont découvert des vulnérabilités dans la manière dont la
conception des processeurs Intel a implémenté la transmission spéculative de
données chargées dans des structures microarchitecturales temporaires (tampons).
Ce défaut pourrait permettre à un attaquant contrôlant un processus non
privilégié de lire des informations sensibles, y compris à partir du noyau et de
tous les autres processus exécutés sur le système ou à travers les limites
clients/hôtes pour lire la mémoire de l'hôte.</p>

<p>Voir <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">\
https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
pour plus de détails.</p>

<p>Pour résoudre complètement ces vulnérabilités il est également
nécessaire d'installer le microcode du processeur mis à jour. Un paquet du
microcode d'Intel mis à jour (seulement disponible dans Debian non-free)
sera fourni à l'aide d'une annonce de sécurité particulière. Le microcode
du processeur mis à jour peut aussi être disponible dans le cadre d'une
mise à jour du microprogramme système (« BIOS »).</p>

<p>En complément, cette mise à jour fournit un correctif pour une
régression provoquant des blocages dans le pilote du service loopback,
introduite dans la mise à jour vers 4.9.168 dans la dernière annonce de
sécurité.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.168-1+deb9u2~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1787.data"
# $Id: $
