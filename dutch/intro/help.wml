#use wml::debian::template title="Hoe kunt u Debian helpen?"
#use wml::debian::translation-check translation="e42e160ab216bcca146be3acec084557e701ceec"

### TRANSLATORS: Revision 1.27 is adding some words to the popcon submission item,
### and Revision 1.28 is reorganization of items (no change in the description of
### each item, just adding title sections and per section unnumbered lists.
### see #872667 for details and the patches.

<p>Indien u erover denkt om te helpen bij het ontwikkelen van Debian,
dan zijn er veel domeinen waaraan zowel ervaren als minder ervaren gebruikers
kunnen meewerken:</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..

<h3>Coderen</h3>

<ul>
<li>Toepassingen waarmee u veel ervaring heeft en die u waardevol acht
voor Debian, kunt u verpakken, en de onderhouder van die pakketten worden.
Raadpleeg de <a href="$(HOME)/devel/">Debian ontwikkelaarshoek</a> voor
extra informatie.</li>
<li>U kunt helpen bij het <a href="https://security-tracker.debian.org/tracker/data/report">opvolgen</a>,
<a href="$(HOME)/security/audit/">zoeken</a> en
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">repareren</a> van
<a href="$(HOME)/security/">veiligheidsproblemen</a> in pakketten van Debian.
U kunt ook helpen bij het minder kwetsbaar maken van
<a href="https://wiki.debian.org/Hardening">pakketten</a>, van
<a href="https://wiki.debian.org/Hardening/RepoAndImages">pakketbronnen en images</a>
en van <a href="https://wiki.debian.org/Hardening/Goals">andere zaken</a>.
</li>
<li>U kunt helpen bij het onderhouden van toepassingen die reeds onderdeel zijn
van het Debian besturingssysteem, in het bijzonder deze welke u veel gebruikt
en waarover u veel weet, door voor deze pakketten in het
<a href="https://bugs.debian.org/">Bugvolgsysteem</a> te helpen bij reparaties
(patches) of door bijkomende informatie aan te dragen.
U kunt ook rechtstreeks betrokken worden bij het onderhoud van pakketten
door aan te sluiten bij een team dat in groep pakketten onderhoudt, of
u engageren voor software die voor Debian ontwikkeld wordt, door aan te
sluiten bij een softwareproject op <a href="https://salsa.debian.org/">Salsa</a>.</li>
<li>U kunt helpen bij het geschikt maken van Debian voor een architectuur
waarmee u ervaring heeft, door een zogenaamde <em>port</em> op te starten
of door bij te dragen aan bestaande ports. Raadpleeg voor bijkomende
informatie de <a href="$(HOME)/ports/">lijst van bestaande ports</a>.</li>
<li>U kunt helpen bij het verbeteren van <a href="https://wiki.debian.org/Services">bestaande</a>
diensten die te maken hebben met Debian of nieuwe diensten opzetten waaraan
de Debian-gemeenschap
<a href="https://wiki.debian.org/Services#wishlist">behoefte heeft</a>.
</ul>

<h3>Testen</h3>

<ul>
<li>U kunt het besturingssysteem en de programma's die het bevat gewoon
testen en eventuele nog niet-gekende fouten of bugs die u vindt, melden
via het <a href="https://bugs.debian.org/">Bugvolgsysteem</a>. Probeer
ook door de bugs te bladeren die verband houden met de door u gebruikte
pakketten, en bijkomende informatie te bezorgen indien u de erin
beschreven problemen kunt reproduceren.</li>
<li>U kunt helpen met het <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">testen van het installatiesysteem en de live ISO-images</a>.
</ul>

<h3>Gebruikersondersteuning</h3>
<ul>
# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<li>Indien u een ervaren gebruiker bent, kunt u andere gebruikers helpen
via de <a href="$(HOME)/support#mail_lists">mailinglijsten voor gebruikers</a>
of via het IRC-kanaal <tt>#debian</tt> (in het Engels) of <tt>#debian-nl</tt> (in het Nederlands). Lees voor bijkomende informatie over
ondersteuningsmogelijkheden en beschikbare ondersteuningsbronnen de
<a href="$(HOME)/support">pagina's in verband met ondersteuning</a>.</li>
</ul>

<h3>Vertaling</h3>
<ul>
# TBD - link to translators mailing lists
# Translators, link directly to your group's pages
<li>U kunt helpen met het vertalen naar uw taal van toepassingen of van
informatie in verband met Debian (webpagina's, documentatie) door mee te
werken aan een vertaalproject (over het algemeen worden deze besproken
in de <a href="https://lists.debian.org/debian-i18n/">i18n-mailinglijst</a>
en voor het Nederlands in de <a href="https://lists.debian.org/debian-l10n-dutch/">mailinglijst van het Nederlandstalig vertaalteam</a>). Indien er voor uw taal nog geen
internationalisatiegroep actief is, kunt u er zelf een opstarten.
Lees de <a href="$(HOME)/international/">Internationalisatiepagina's</a>
voor meer informatie.</li>
</ul>

<h3>Documentatie</h3>
<ul>
<li>U kunt helpen bij het schrijven van documentatie door te werken aan
de officiële documentatie die verzorgd wordt door het <a href="$(HOME)/doc/ddp">Documentatieproject van Debian</a> of door bijdragen te schrijven voor
de <a href="https://wiki.debian.org/">Wikipagina's van Debian</a>.</li>
<li>U kunt pakketten labelen en categoriseren op de website
<a href="https://debtags.debian.org/">debtags</a>, zodat het voor onze
gebruikers makkelijker wordt de software te vinden waarnaar ze op zoek zijn.</li>
</ul>

<h3>Evenementen</h3>
<ul>
<li>U kunt helpen bij het vormen van het <em>publieke</em> gezicht van Debian
door bij te dragen aan de <a href="$(HOME)/devel/website/">website</a> of
door te helpen met het organiseren van over de hele wereld verspreide <a href="$(HOME)/events/">evenementen</a>.</li>
<li>Help Debian zichzelf te promoten door erover te praten en het aan
anderen te demonstreren.</li>
<li>Help een <a href="https://wiki.debian.org/LocalGroups">lokale Debian-groep</a> te creëren of help bij het organiseren van regelmatige bijeenkomsten
en andere activiteiten.</li>
<li>
U kunt meehelpen aan de jaarlijkse <a href="http://debconf.org/">Conferentie van Debian</a>,
onder meer met de <a href="https://wiki.debconf.org/wiki/Videoteam">video-opnames van de toespraken</a>, met het
<a href="https://wiki.debconf.org/wiki/FrontDesk">verwelkomen van de deelnemers bij hun aankomst</a>, met het
<a href="https://wiki.debconf.org/wiki/Talkmeister">helpen van de sprekers vooraleer die aan hun toespraak beginnen</a>, met
speciale evenementen (zoals de kaas- en wijnavond), met opbouwen, afbreken en andere zaken.
</li>
<li>
U kunt de jaarlijkse <a href="http://debconf.org/">Conferentie van Debian</a> helpen organiseren, of helpen bij een mini-conferentie (mini-DebConf) in uw regio, bij
<a href="https://wiki.debian.org/DebianDay">Debian Day-feestjes</a>, bij
<a href="https://wiki.debian.org/ReleaseParty">releaseparty's</a>, bij
<a href="https://wiki.debian.org/BSP">bugopruimingsparty's</a>, bij
<a href="https://wiki.debian.org/Sprints">ontwikkelingssprints</a> en bij
<a href="https://wiki.debian.org/DebianEvents">andere evenementen</a> overal ter wereld.
</li>
</ul>

<h3>Schenkingen</h3>
<ul>
<li>U kunt <a href="$(HOME)/donations">geld, uitrusting en diensten schenken</a>
aan het Debian project, zodat zijn gebruikers of ontwikkelaars er kunnen van
genieten. Wij zijn wereldwijd permanent op zoek naar
<a href="$(HOME)/mirror/">spiegelservers</a> waarop onze gebruikers kunnen vertrouwen en naar
<a href="$(HOME)/devel/buildd/">auto-builder-systemen</a> voor onze ontwikkelaars van ports.</li>
</ul>

<h3>Gebruik Debian!</h3>
<ul>
<li>
U kunt <a href="https://wiki.debian.org/ScreenShots">schermafdrukken maken</a>
van pakketten en deze <a href="https://screenshots.debian.net/upload">uploaden</a> naar
<a href="https://screenshots.debian.net/">screenshots.debian.net</a>, zodat
onze gebruikers kunnen zien hoe software in Debian eruit ziet, vooraleer ze
deze gaan gebruiken.
</li>
<li>U kunt de <a href="https://packages.debian.org/popularity-contest">participatie aan de populariteitsmeting</a> activeren, zodat we
weten welke pakketten populair zijn en voor iedereen het meest nuttig.</li>
</ul>

<p>Zoals u kunt zien zijn er veel manieren om betrokken te geraken bij
het project en slechts voor enkele ervan moet u een ontwikkelaar van Debian
zijn. Veel van de projecten hebben mechanismes die rechtstreekse toegang
tot de broncode mogelijk maken voor personen welke een bijdrage leveren en
die hebben aangetoond dat ze betrouwbaar en waardevol zijn. Gewoonlijk zullen
personen die wensen meer betrokken te worden bij Debian, <a
href="$(HOME)/devel/join">zich aansluiten bij het project</a>, maar dit is
niet steeds nodig.</p>

<h3>Organisaties</h3>

<p>
Mogelijk is uw onderwijsinstelling, uw overheidsdienst of uw commerciële of
non-profitorganisatie erin geïnteresseerd Debian te helpen gebruikmaken
van de middelen waarover ze beschikt.
Uw instelling of organisatie kan
<a href="https://www.debian.org/donations">schenkingen doen aan ons</a>,
<a href="https://www.debian.org/partners/">een langdurig partnerschap aangaan met ons</a>,
<a href="https://www.debconf.org/sponsors/">onze conferenties sponsoren</a>,
<a href="https://wiki.debian.org/MemberBenefits">gratis producten of diensten leveren aan medewerkers van Debian</a>,
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">gratis hosting bieden voor service-experimenten van Debian</a>,
spiegelservers opzetten voor onze
<a href="https://www.debian.org/mirror/ftpmirror">software</a>,
<a href="https://www.debian.org/CD/mirroring/">installatiemedia</a>
en <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">conferentievideo's</a>,
of onze software en onze gemeenschap promoten door
<a href="https://www.debian.org/users/">erover te getuigen</a>
of <a href="https://www.debian.org/events/merchandise">koopwaar</a>,
<a href="https://www.debian.org/CD/vendors/">installatiemedia</a>,
<a href="https://www.debian.org/distrib/pre-installed">vooraf geïnstalleerde systemen</a> van Debian te verkopen, of
<a href="https://www.debian.org/consultants/">consultancy</a> of
<a href="https://wiki.debian.org/DebianHosting">hosting</a> te bieden.
</p>

<p>
U zou ook kunnen helpen uw staf aan te moedigen zich in onze gemeenschap te engageren door hen met Debian in contact te brengen via het gebruik van ons besturingssysteem in uw organisatie, door hen les te geven over het Debian besturingssysteem en de Debian gemeenschap, door hen te begeleiden om tijdens de werkuren met ons mee te werken of door hen naar onze <a href="$(HOME)/events/">evenementen</a> te sturen.
</p>

# <p>Related links:
